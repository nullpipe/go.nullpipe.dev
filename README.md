# go.nullpipe.dev

The go.nullpipe.dev website which uses [vangen](https://github.com/leighmcculloch/vangen) and GitLab Pages to host Go vanity imports as a static site.  For example, [zilch](https://go.nullpipe.dev/zilch).

## Generate

From this main project directory.

```sh
vangen -out=public/
```
